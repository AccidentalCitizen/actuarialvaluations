﻿using AML.Domain;
using AML.infrastructure.Data.Dto;
using AML.Infrastructure.Data.EF;
using AML.Infrastructure.Readers;
using AML.Infrastructure.Writers;
using SKF.Domain.Tests.Data;
using SKF.Infrastructure.Connections;
using System;
using System.Collections.Generic;

namespace TestEXE
{
    class Program
    {
        static void Main(string[] args)
        {
            var csvConnection = new CsvDataReaderConnection<SimpleDto>(@"C:\Users\rapillay\Documents\Projects\AI\actuarialvaluations\MarkovCsv.csv");
            var data = csvConnection.LoadData();
            var writer = new GeneralCsvDataToStringOnlyDbWriter(csvConnection
                , "[data].[MarkovRateInputValues]"
                , @"data source=5CD6211Z8J\SQLEXPRESS;initial catalog=ActuarialValuations;integrated security=True;MultipleActiveResultSets=True");
            writer.Write();
            var markovStateReader = new MarkovStateTransitionRateValuesByModelIDReader(new ActuarialValuationsEntities());
            var readerData = markovStateReader.GetData("1E22E1E1E2E2");
            var markovStates = readerData;// DomainData.MarkovStateValues();
            var benefits = DomainData.BenefitValues();
            var resultProjections = Projections.GetAllTransitionStateValueProjectionsUpToPeriod(markovStates, new List<string>() { "1E22E1E1E2E2"}, 5);
            var benefitProjections = BenefitProjections.ProjectBenefits(markovStates, benefits, new List<string>() { "1E22E1E1E2E2" }, 5);
            foreach (var projection in resultProjections)
            {
                Console.WriteLine("{0} {1} {2}", projection.Period, projection.outRates,projection.ExposureValues);
            }
            foreach(var benefitProjection in benefitProjections)
            {
                Console.WriteLine("{0} {1} {2}", benefitProjection.Period, benefitProjection.MarkovState, benefitProjection.BenefitProjectedValue);
            }
        }
    }
}
