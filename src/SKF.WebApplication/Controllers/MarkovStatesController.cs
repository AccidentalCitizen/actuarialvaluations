﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AML.infrastructure.Data.Dto;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SKF.WebApplication.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SKF.WebApplication.Controllers
{
    public class MarkovStatesController : Controller
    {


        private readonly IHostingEnvironment _appEnvironment;

        public MarkovStatesController(IHostingEnvironment appEnvironment)

        {

            //----< Init: Controller >----

            _appEnvironment = appEnvironment;

            //----</ Init: Controller >----

        }



        // GET: /<controller>/
        public IActionResult Index()
        {

            MarkovStatesDto objMarkovStatesDto = new MarkovStatesDto()
            {

                Applicable_Model_ID = 100,
                From_State_ID = 1,
                To_State_ID = 2,
                Period = 1,
                Value = 0.01m
            };
            //var obj = new MarkovStatesWriter.Write(objMarkovStatesDto);

            return View();
        }
        [HttpGet]
        public IActionResult CreateMarkovStates()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateMarkovStates(MarkovStatesModel obj)
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UploadCSVFile1(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);

            // full path to file in temp location
            var filePath = Path.GetTempFileName();

            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                }
            }

            // process uploaded files
            // Don't rely on or trust the FileName property without validation.

            return Ok(new { count = files.Count, size, filePath });
        }


       [HttpPost] //Postback
        public async Task<IActionResult> UploadCSVFile(IFormFile file)
        {

            //--------< Upload_ImageFile() >--------

            //< check >

            if (file == null || file.Length == 0) return Content("file not selected");

            //</ check >

            List<MarkovStatesModel> markovStatesModel = new List<MarkovStatesModel>();
            var filepath = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot",
                        file.FileName);

            using (var stream = new FileStream(filepath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }


            //Read the contents of CSV file.
            string csvData = System.IO.File.ReadAllText(filepath);

            //Execute a loop over the rows.
            foreach (string row in csvData.Split('\n'))
            {
                if (!string.IsNullOrEmpty(row))
                {
                    markovStatesModel.Add(new MarkovStatesModel
                    {
                         To_StateID = Convert.ToInt32(row.Split(',')[0]),
                        From_StateID = Convert.ToInt32(row.Split(',')[1]),
                        Period = Convert.ToInt32(row.Split(',')[2]),
                        Value= Convert.ToDecimal(row.Split(',')[3]),
                    });
                }
            }
            //< output >

            ViewData["FilePath"] = filepath;

            return View();

            //</ output >

            //--------</ Upload_ImageFile() >--------

        }
    }


}
