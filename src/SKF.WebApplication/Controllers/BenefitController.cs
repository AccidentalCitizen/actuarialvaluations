﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AML.Infrastructure.Data.EF;
using Microsoft.AspNetCore.Mvc;
using SKF.WebApplication.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SKF.WebApplication.Controllers
{
    public class BenefitController : Controller
    {
        //private readonly ActuarialValuationsEntities _aiValuationsContext;
        //public BenefitController(ActuarialValuationsEntities aiValuationsContext)
        //{
        //    _aiValuationsContext = aiValuationsContext;
        //}
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            //List<Benefit> lst = new List<Benefit>();
            //lst = (from benefit in _aiValuationsContext.Benefits
            //       select benefit).ToList();
            return View();
        }

        [HttpPost]
        public IActionResult Create(BenefitModel obj)
        {
            return View();
        }
    }
}
