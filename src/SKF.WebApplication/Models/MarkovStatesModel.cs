﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SKF.WebApplication.Models
{
    public class MarkovStatesModel
    {
        
        public int Applicable_ModelID { get; set; }
        public int To_StateID { get; set; }
        public int From_StateID { get; set; }
        public int Period { get; set; }
        public decimal Value { get; set; }
    }
}
