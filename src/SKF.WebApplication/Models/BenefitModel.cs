﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SKF.WebApplication.Models
{
    public class BenefitModel
    {
        public int ID { get; set; }
        public Nullable<int> AssociatedMarkovStateID { get; set; }
        public string BenefitType { get; set; }
        public Nullable<bool> AddSubtract { get; set; }
        public Nullable<decimal> Value { get; set; }
    }
}
