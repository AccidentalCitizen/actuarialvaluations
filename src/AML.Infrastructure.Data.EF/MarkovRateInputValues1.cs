//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AML.Infrastructure.Data.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class MarkovRateInputValues1
    {
        public int ID { get; set; }
        public Nullable<int> From_State_ID { get; set; }
        public Nullable<int> To_State_ID { get; set; }
        public Nullable<int> PeriodMonthlyIncrements { get; set; }
        public Nullable<decimal> IndependantValue { get; set; }
        public Nullable<int> AgeID { get; set; }
        public Nullable<int> IndustryID { get; set; }
        public Nullable<int> EducationLevelID { get; set; }
        public Nullable<int> SmokerStatusID { get; set; }
        public Nullable<int> GenderID { get; set; }
    
        public virtual MarkovState MarkovState { get; set; }
        public virtual MarkovState MarkovState1 { get; set; }
        public virtual age age { get; set; }
        public virtual Gender Gender { get; set; }
        public virtual Industry Industry { get; set; }
        public virtual SmokerStatu SmokerStatu { get; set; }
    }
}
