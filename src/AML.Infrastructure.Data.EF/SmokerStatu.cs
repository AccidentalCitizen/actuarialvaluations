//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AML.Infrastructure.Data.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class SmokerStatu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SmokerStatu()
        {
            this.MarkovRateInputValues1 = new HashSet<MarkovRateInputValues1>();
        }
    
        public int ID { get; set; }
        public Nullable<bool> Status { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MarkovRateInputValues1> MarkovRateInputValues1 { get; set; }
    }
}
