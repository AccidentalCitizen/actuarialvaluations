//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AML.Infrastructure.Data.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class MarkovState
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MarkovState()
        {
            this.DependantMarkovStates = new HashSet<DependantMarkovState>();
            this.DependantMarkovStates1 = new HashSet<DependantMarkovState>();
            this.MarkovRateInputValues1 = new HashSet<MarkovRateInputValues1>();
            this.MarkovRateInputValues11 = new HashSet<MarkovRateInputValues1>();
            this.MarkovStates1 = new HashSet<MarkovStates1>();
            this.MarkovStates11 = new HashSet<MarkovStates1>();
        }
    
        public int ID { get; set; }
        public string StateName { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DependantMarkovState> DependantMarkovStates { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DependantMarkovState> DependantMarkovStates1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MarkovRateInputValues1> MarkovRateInputValues1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MarkovRateInputValues1> MarkovRateInputValues11 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MarkovStates1> MarkovStates1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MarkovStates1> MarkovStates11 { get; set; }
    }
}
