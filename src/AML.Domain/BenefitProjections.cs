﻿using System.Collections.Generic;
using System.Linq;

namespace AML.Domain
{
    public class BenefitProjections
    {
        public string BenefitName { get; private set; }
        public decimal BenefitProjectedValue { get; private set; }
        public int Period { get; private set; }
        public int MarkovState { get; private set; }

        public BenefitProjections(string BenefitName,
                                   decimal BenefitProjectedValue,
                                   int Period,
                                   int MarkovState)
        {
            this.BenefitName = BenefitName;
            this.BenefitProjectedValue = BenefitProjectedValue;
            this.Period = Period;
            this.MarkovState = MarkovState;
        }
        public static IList<BenefitProjections> ProjectBenefits(IList<MarkovStates> statesByModelID,
            IList<Benefits> benefits,
            IList<string> applicableModelIds, int period)
        {
            var returnType = new List<BenefitProjections>();
            foreach (var benefit in benefits)
            {
                for (int i = 0; i < period; i++)
                {
                    foreach (var modelId in applicableModelIds)
                    {
                        var state = statesByModelID
                            .SingleOrDefault(s => s.ID == benefit.AssociatedMarkovStateID
                            &&
                            s.Applicable_Model_ID == modelId
                            && s.PeriodMonthlyIncrements == i);
                        if (state != null)
                        {
                            returnType.Add(new BenefitProjections(benefit.BenefitType
                                , (decimal)(state.IndependantValue * benefit.Value), i, state.ID));
                        }
                    }
                }
            }
            return returnType;
        }
    }
}
