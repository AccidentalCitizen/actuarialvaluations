﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AML.Domain
{
    public class MarkovStates
    {
        public int ID { get; set; }
        public string Applicable_Model_ID { get; set; }
        public Nullable<int> From_State_ID { get; set; }
        public Nullable<int> To_State_ID { get; set; }
        public IList<States> To_Multiple_Out_States { get; set; }
        public MarkovStateDependant DependantMarkovStates { get; set; }
        public Nullable<int> PeriodMonthlyIncrements { get; set; }
        public decimal IndependantValue { get; set; }
        public decimal DependantValue { get { return GetDependantValue(); } }
        public decimal AllOutRatesMultiplied()
        {
            var retValue = IndependantValue;
            if(To_Multiple_Out_States != null)
            {
                foreach(var val in To_Multiple_Out_States)
                {
                    retValue = retValue * val.Value;
                }
            }
            return retValue;
        }

        public decimal GetDependantValue()
        {
            var factor = 1m;
            foreach (var dependantMarkovIDandValuePair in 
                DependantMarkovStates.DependantOnMarkovStateIDandValuePairs)
            {
                factor = factor * (1 - dependantMarkovIDandValuePair.Yval * 
                    To_Multiple_Out_States.Single(s=>s.OutStateID == dependantMarkovIDandValuePair.Xval).Value);
            }
            return factor;
        }

    }

    public class States
    {
        public States(int OutStateID,
                      decimal Value)
        {
            this.OutStateID = OutStateID;
            this.Value = Value;
        }
        public int OutStateID { get; private set; }
        public decimal Value { get; private set; }
    }
}
