﻿

namespace AML.Domain.Enums
{
    public enum TimeIncrementTypes
    {
        MilliSecond,
        Second,
        Minute,
        Hour
    }
}
