﻿

namespace AML.Domain.Enums
{
    public enum CustomerTimePeriods
    {
        Yearly,
        Monthly,
        Daily
    }
}
