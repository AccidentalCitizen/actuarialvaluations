﻿

namespace AML.Domain.Enums
{
    public enum DateIncrementTypes
    {
        Year,
        Month,
        Day,
    }
}
