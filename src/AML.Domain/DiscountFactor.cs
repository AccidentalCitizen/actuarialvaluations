﻿using System;

namespace AML.Domain
{
    public class DiscountFactor
    {
        private static decimal discountFactorValue(decimal yieldRate, decimal term)
        {
            var discountFactor = Math.Pow((1 / (double)(1 + yieldRate)), (double)(term));
            return (decimal)discountFactor;
        }
    }
}
