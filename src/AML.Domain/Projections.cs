﻿using System.Collections.Generic;
using System.Linq;

namespace AML.Domain
{
    public class Projections
    {
        public decimal ExposureValues { get; private set; }
        public int Period { get; private set; }
        public decimal outRates { get; private set; }
        public Projections( decimal ExposureValues,
                            int Period,
                            decimal outRates)
        {
            this.ExposureValues = ExposureValues;
            this.Period = Period;
            this.outRates = outRates;
        }
        /// <summary>
        /// This object is responsible for projecting policy count at each projection period 
        /// taking into account all configured Markov States
        /// </summary>
        /// <param name="statesByModelID"></param>
        /// <param name="upToPeriod"></param>
        /// <returns></returns>
        public static IList<Projections> GetAllTransitionStateValueProjectionsUpToPeriod
            (IList<MarkovStates> statesByModelID, IList<string> applicableModelIds, int upToPeriod)
        {
            var returnType = new List<Projections>();
            returnType.Add(new Projections(1,0,0));
            for (int i = 1; i <= upToPeriod; i++)
            {var sumAllRates = 0m;
                foreach (var modelId in applicableModelIds)
                {
                    var stateRatesUpToPeriod = statesByModelID.Where(w => w.PeriodMonthlyIncrements == i 
                    && w.Applicable_Model_ID == modelId);

                    foreach (var markovState in stateRatesUpToPeriod)
                    {
                        sumAllRates += markovState.IndependantValue;
                    }
                    
                }
                returnType.Add(new Projections((returnType[i - 1].ExposureValues - sumAllRates), i, sumAllRates));
            }
            return returnType;
        }

        /// <summary>
        /// This object is responsible for projecting policy count at each projection period 
        /// taking into account all configured Markov States
        /// </summary>
        /// <param name="statesByModelID"></param>
        /// <param name="upToPeriod"></param>
        /// <returns></returns>
        public static IList<Projections> GetAllMultiStateTransitionProjectionsUpToPeriod
            (IList<MarkovStates> statesByModelID, IList<string> applicableModelIds, int upToPeriod)
        {
            var returnType = new List<Projections>();
            returnType.Add(new Projections(1, 0, 0));
            for (int i = 1; i <= upToPeriod; i++)
            {
                var sumAllRates = 0m;
                foreach (var modelId in applicableModelIds)
                {
                    var stateRatesUpToPeriod = statesByModelID.Where(w => w.PeriodMonthlyIncrements == i
                    && w.Applicable_Model_ID == modelId);

                    foreach (var markovState in stateRatesUpToPeriod)
                    {
                        sumAllRates += markovState.AllOutRatesMultiplied();
                    }

                }
                returnType.Add(new Projections((returnType[i - 1].ExposureValues - sumAllRates), i, sumAllRates));
            }
            return returnType;
        }
    }
}
