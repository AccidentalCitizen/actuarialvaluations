﻿using System;

namespace AML.Domain
{
    public class Benefits
    {
        public int ID { get; set; }
        public Nullable<int> AssociatedMarkovStateID { get; set; }
        public string BenefitType { get; set; }
        public Nullable<bool> AddSubtract { get; set; }
        public Nullable<decimal> Value { get; set; }
    }
}
