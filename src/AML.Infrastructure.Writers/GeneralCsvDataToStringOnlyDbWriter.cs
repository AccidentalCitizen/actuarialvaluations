﻿using AML.infrastructure.Data.Dto;
using SKF.Infrastructure.Connections.Interfaces;
using SKF.Infrastructure.Writers.Interfaces;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace AML.Infrastructure.Writers
{
    public class GeneralCsvDataToStringOnlyDbWriter : IDataWrite
    {
        private readonly IDataReadConnection<IList<SimpleDto>> con;
        private string tableName;
        private string dbConnectionString;
        public GeneralCsvDataToStringOnlyDbWriter(IDataReadConnection<IList<SimpleDto>> con,
            string tableName, string dbConnectionString)
        {
            this.con = con;
            this.tableName = tableName;
            this.dbConnectionString = dbConnectionString;
        }

        public void Write()
        {
            var connection = new SqlConnection(dbConnectionString);
            connection.Open();
            var cmd = new SqlCommand();
            cmd.Connection = connection;
            var result = con.LoadData();
            var insertStatementList = new List<string>();
            foreach (var row in result)
            {
                var count = row.row.Length;
                int cntr = 0;
                
                var insertStatement = "INSERT INTO " + tableName + " VALUES (";
                foreach (var item in row.row)
                {
                    if (cntr<=(count-2))
                    {
                        insertStatement += "'" + item + "',";
                    }
                    else
                    {
                        insertStatement += "'" + item + "'";
                    }
                    cntr++;
                }
                insertStatementList.Add(insertStatement + ");");
            }
            foreach (var statement in insertStatementList)
            {
                cmd.CommandText = statement;
                cmd.ExecuteNonQuery();
            }
            
            //throw new System.NotImplementedException();
        }
    }
}
