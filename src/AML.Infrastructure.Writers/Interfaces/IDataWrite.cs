﻿namespace SKF.Infrastructure.Writers.Interfaces
{
    public interface IDataWrite<P>
    {
        void Write(P values);
    }

    public interface IDataWrite
    {
        void Write();
    }
}
