﻿

using FileHelpers;

namespace AML.infrastructure.Data.Dto
{
    [DelimitedRecord(",")]
    public class SimpleDto
    {
        [FieldDelimiter(",")]
        public string[] row;
    }
}
