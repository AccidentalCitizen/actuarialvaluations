﻿using AML.Domain;
using AML.Domain.Container_Objects;
using AML.Infrastructure.Data.EF;
using SKF.Infrastructure.Mapper;
using SKF.Infrastructure.Readers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AML.Infrastructure.Readers
{
    public class MarkovStateTransitionRateValuesByModelIDReader : IDataReaderStringIdParametric<IList<MarkovStates>>
    {
        private readonly ActuarialValuationsEntities context;
        public MarkovStateTransitionRateValuesByModelIDReader(ActuarialValuationsEntities context)
        {
            this.context = context;
        }

        public void ClearAndDispose()
        {
            throw new NotImplementedException();
        }

        public IList<MarkovStates> GetData(string stringId)
        {
            var returnType = new List<MarkovStates>();
            var config = Mapper.CreateMapAndReturnConfig<MarkovStates1, MarkovStates>();
            var mapper = config.CreateMapper();
            var results = context.MarkovStates1.Where(w => w.Applicable_Model_ID == stringId);
            var dependantMarkov = context.DependantMarkovStates.Where(w => w.ModelID == stringId);
            foreach (var state in results)
            {
                var markov = mapper.Map<MarkovStates1, MarkovStates>(state);
                markov.To_Multiple_Out_States = PopulateStates(results);
                markov.DependantMarkovStates = new MarkovStateDependant()
                {
                    DependantMarkovStateID = 1,
                    DependantOnMarkovStateIDandValuePairs = GetAllDependantStates(dependantMarkov),
                    ModelID = stringId,
                    Value = dependantMarkov.First().Value

                };
                returnType.Add(markov);
            }
            return returnType;
        }

        private static List<States> PopulateStates(IQueryable<MarkovStates1> results)
        {
            var returnType = new List<States>();
            var config = Mapper.CreateMapAndReturnConfig<MarkovStates1, MarkovStates>();
            var mapper = config.CreateMapper();
            foreach (var state in results)
            {
                returnType.Add(new States(state.To_State_ID,state.IndependantValue));
            }
            return returnType;
        }

        private static List<Point<int, decimal>> GetAllDependantStates(IQueryable<DependantMarkovState> 
            dependantMarkov)
        {
            var list = new List<Point<int, decimal>>();
            foreach(var dependantState in dependantMarkov)
            {
                list.Add(new Point<int, decimal>((int)dependantState.DependantOnMarkovStateID
                    ,(decimal)dependantState.Value));
            }
            return list;
        }
    }
}
