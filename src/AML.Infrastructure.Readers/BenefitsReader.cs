﻿using AML.Domain;
using AML.Infrastructure.Data.EF;
using SKF.Infrastructure.Mapper;
using SKF.Infrastructure.Readers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AML.Infrastructure.Readers
{
    public class BenefitsReader : IDataReaderIdParametric<IList<Benefits>>
    {
        private readonly ActuarialValuationsEntities context;
        public BenefitsReader(ActuarialValuationsEntities context)
        {
            this.context = context;
        }

        public void ClearAndDispose()
        {
            throw new NotImplementedException();
        }

        public IList<Benefits> GetData(int id)
        {
            var returnType = new List<Benefits>();
            var config = Mapper.CreateMapAndReturnConfig<Benefit, Benefits>();
            var mapper = config.CreateMapper();
            var results = context.Benefits.Where(w => w.ID == id);
            foreach (var state in results)
            {
                returnType.Add(mapper.Map<Benefit, Benefits>(state));
            }
            return returnType;
        }
    }
}
