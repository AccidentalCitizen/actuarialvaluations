﻿using AutoMapper;
namespace SKF.Infrastructure.Mapper
{
    /// <summary>
    /// For use in mapping Dto to Domain Objects at reader level.
    /// </summary>
    public class Mapper
    {
        public static MapperConfiguration CreateMapAndReturnConfig<Source, Dest>()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Source, Dest>();
            });
            return config;
        }
    }

    //var config = Mappings.Mappings.CreateMapAndReturnConfig<T, U>();
    //var configExt = Mappings.Mappings.CreateMapAndReturnConfig<T, V>();
    //var mapper = config.CreateMapper();
    //var mapperExt = configExt.CreateMapper();
    //var cnt = 0;
    //listOut = new List<U>();
    //        extListOut = new List<V>();
    //        foreach (var obj in list)
    //        {
    //            listOut.Add(mapper.Map<T, U>(obj));
    //            extListOut.Add(mapperExt.Map<T, V>(extList[cnt]));
    //            cnt++;
    //        }
}
