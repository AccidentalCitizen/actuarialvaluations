﻿using System.Collections.Generic;
using AML.Domain;

namespace SKF.Domain.Tests.Data
{

    public class DomainData
    {
        public static IList<MarkovStates> MarkovStateValues()
        {
            return new List<MarkovStates>()
            {
                new MarkovStates()
                {
                    ID =1,
                    Applicable_Model_ID =200.ToString(),
                    From_State_ID =1,
                    To_State_ID =2,
                    PeriodMonthlyIncrements =1 ,
                    IndependantValue = 0.01m
                },
                new MarkovStates()
                {
                    ID =2,
                    Applicable_Model_ID =200.ToString(),
                    From_State_ID =1,
                    To_State_ID =3,
                    PeriodMonthlyIncrements =1 ,
                    IndependantValue = 0.02m
                },
                new MarkovStates()
                {
                    ID =1,
                    Applicable_Model_ID =100.ToString(),
                    From_State_ID =1,
                    To_State_ID =2,
                    PeriodMonthlyIncrements =1 ,
                    IndependantValue = 0.01m 
                },
                new MarkovStates()
                {
                    ID =2,
                    Applicable_Model_ID =100.ToString(),
                    From_State_ID =1,
                    To_State_ID =3,
                    PeriodMonthlyIncrements =1 ,
                    IndependantValue = 0.02m
                },
                new MarkovStates()
                {
                    ID =1,
                    Applicable_Model_ID =100.ToString(),
                    From_State_ID =1,
                    To_State_ID =2,
                    PeriodMonthlyIncrements =2 ,
                    IndependantValue = 0.01m
                },
                new MarkovStates()
                {
                    ID =2,
                    Applicable_Model_ID =100.ToString(),
                    From_State_ID =1,
                    To_State_ID =3,
                    PeriodMonthlyIncrements =2 ,
                    IndependantValue = 0.02m
                },
                new MarkovStates()
                {
                    ID =1,
                    Applicable_Model_ID =100.ToString(),
                    From_State_ID =1,
                    To_State_ID =2,
                    PeriodMonthlyIncrements =3 ,
                    IndependantValue = 0.01m
                },
                new MarkovStates()
                {
                    ID =2,
                    Applicable_Model_ID =100.ToString(),
                    From_State_ID =1,
                    To_State_ID =3,
                    PeriodMonthlyIncrements =3 ,
                    IndependantValue = 0.02m
                },
                new MarkovStates()
                {
                    ID =1,
                    Applicable_Model_ID =100.ToString(),
                    From_State_ID =1,
                    To_State_ID =2,
                    PeriodMonthlyIncrements =4 ,
                    IndependantValue = 0.01m
                },
                new MarkovStates()
                {
                    ID =2,
                    Applicable_Model_ID =100.ToString(),
                    From_State_ID =1,
                    To_State_ID =3,
                    PeriodMonthlyIncrements =4 ,
                    IndependantValue = 0.02m
                },
                new MarkovStates()
                {
                    ID =1,
                    Applicable_Model_ID =100.ToString(),
                    From_State_ID =1,
                    To_State_ID =2,
                    PeriodMonthlyIncrements =5 ,
                    IndependantValue = 0.01m
                },
                new MarkovStates()
                {
                    ID =2,
                    Applicable_Model_ID =100.ToString(),
                    From_State_ID =1,
                    To_State_ID =3,
                    PeriodMonthlyIncrements =5 ,
                    IndependantValue = 0.02m
                }
            };
        }

        public static IList<Benefits> BenefitValues()
        {
            return new List<Benefits>()
            {
                new Benefits(){  AddSubtract = false, AssociatedMarkovStateID = 1, BenefitType ="Test", ID = 1, Value = 2000},
                new Benefits(){  AddSubtract = false, AssociatedMarkovStateID = 2, BenefitType ="Test", ID = 2, Value = 3500},
            };
        }
    }
}
