﻿using AML.Domain;
using NUnit.Framework;
using SKF.Domain.Tests.Data;
using System.Collections.Generic;

namespace SKF.Domain.Tests.Benefit_Tests
{
    [TestFixture]
    [Category("Domain")]
    public class BenefitTests
    {
        private IList<MarkovStates> markovStates;
        private IList<Benefits> benefits;
        [SetUp]
        public void BeforeEachTest()
        {
            markovStates = DomainData.MarkovStateValues();
            benefits = DomainData.BenefitValues();
        }
        [TestCase]
        public void AssertAllExposureProjectionsAccurate()
        {
            var resultProjections = BenefitProjections.ProjectBenefits(markovStates, benefits,
                new List<string>() { "100" },5);


            Assert.IsTrue(resultProjections[0].Period == 0
                &&
                resultProjections[0].BenefitProjectedValue == 20.00m
                &&
                resultProjections[3].Period == 3
                &&
                resultProjections[3].BenefitProjectedValue == 20.00m
                 &&
                resultProjections[7].Period == 7
                &&
                resultProjections[7].BenefitProjectedValue == 70.00m);
        }
    }
}
