﻿using AML.Domain;
using NUnit.Framework;
using SKF.Domain.Tests.Data;
using System.Collections.Generic;

namespace SKF.Domain.Tests.Projection_Tests
{
    [TestFixture]
    [Category("Domain")]
    public class ProjectionTests
    {
        private IList<MarkovStates> markovStates;
        [SetUp]
        public void BeforeEachTest()
        {
            markovStates = DomainData.MarkovStateValues();
        }
        [TestCase]
        public void AssertAllExposureProjectionsAccurate()
        {
            var resultProjections = Projections.GetAllTransitionStateValueProjectionsUpToPeriod(markovStates,new List<string>() { "100"}, 5);


                Assert.IsTrue(resultProjections[0].Period == 0 
                    &&
                    resultProjections[0].ExposureValues ==  1
                    &&
                    resultProjections[3].Period == 3
                    &&
                    resultProjections[3].ExposureValues == 0.088m
                     &&
                    resultProjections[5].Period == 5
                    &&
                    resultProjections[5].ExposureValues == 0.82m);
        }
    }
}
